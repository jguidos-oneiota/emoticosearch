<?php


namespace App\Controller;

use App\Entity\Like;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Class ApiController
 * @package App\Controller
 */
class ApiController extends AbstractFOSRestController
{
    /**
     * @Rest\Post("/likes")
     */
    public function postLikes(Request $request)
    {
        try {
            $product_id = $request->request->get("product_id");

            if (!$product_id) {
                throw new Exception("product_id is required", 1000);
            }

            $emoticon_id = $request->request->get("emoticon_id");

            if (!$emoticon_id) {
                throw new Exception("emoticon_id is required", 1001);
            }

            $entityManager = $this->getDoctrine()->getManager();

            $like = $this->getDoctrine()
                ->getRepository(Like::class)
                ->findOneBy(["product" => $product_id, "emoticon" => $emoticon_id]);

            if (!$like) {
                $counter = 1;
                $like = new Like();
                $like->setProduct($product_id);
                $like->setEmoticon($emoticon_id);
                $like->setCounter($counter);
            } else {
                $counter = $like->getCounter() + 1;
                $like->setCounter($counter);
            }

            $entityManager->persist($like);
            $entityManager->flush();



            $view = $this->view(["data" => ["product_id" => $like->getProduct(), "emoticon_id" => $like->getEmoticon(), "likes" => $like->getCounter()]], 200);
        } catch (Exception $ex) {
            $view = $this->view(["error" => ["code" => $ex->getCode(), "message" => $ex->getMessage()]], 400);
        }
        return $this->handleView($view);
    }

    /**
     * @Rest\Get("/likes/{product_id}")
     */
    public function getLikes(Request $request, $product_id)
    {
        try {
            $likes = $this->getDoctrine()
                ->getRepository(Like::class)
                ->findBy(["product" => $product_id]);

            $data = ["product_id" => $product_id, "likes" => []];
            foreach ($likes as $like) {
                $data['likes'][] = ["emoticon_id" => $like->getEmoticon(), "likes" => $like->getCounter()];
            }

            $view = $this->view(["data" => $data], 200);

        } catch (Exception $ex) {
            $view = $this->view(["error" => ["code" => $ex->getCode(), "message" => $ex->getMessage()]], 400);
        }
        return $this->handleView($view);
    }

    /**
     * @Rest\Post("/likes/search")
     */
    public function postLikesSearch(Request $request)
    {
        try {
            $products_list = $request->request->get("products_list");

            if (!is_array($products_list) || !$products_list) {
                throw new Exception("products_list is required", 1000);
            }

            $emoticons_list = $request->request->get("emoticons_list");

            if (!is_array($emoticons_list) || !$emoticons_list) {
                throw new Exception("emoticons_list is required", 1001);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $rsm = new ResultSetMappingBuilder($entityManager);
            $rsm->addRootEntityFromClassMetadata('App\Entity\Like', 't1');

            $query = $entityManager->createNativeQuery('SELECT t1.product, t1.emoticon, t1.counter FROM (SELECT t2.* FROM `like` t2 WHERE t2.product IN ('.implode(",", $products_list).')) t1 WHERE t1.emoticon IN ('.implode(",", $emoticons_list).');', $rsm);
            $result = $query->getResult();

            $products_ids = [];

            foreach ($result as $like) {
                if (!in_array($like->getProduct(), $products_ids)) {
                    $products_ids[] = $like->getProduct();
                }
            }

            $data = [];

            foreach ($products_ids as $id) {
                $likes = $this->getDoctrine()
                    ->getRepository(Like::class)
                    ->findBy(["product" => $id]);

                $product = ["product_id" => $id, "likes" => []];
                foreach ($likes as $like) {
                    $product['likes'][] = ["emoticon_id" => $like->getEmoticon(), "likes" => $like->getCounter()];
                }

                $data[] = $product;
            }

            $view = $this->view(["data" => $data], 200);

        } catch (Exception $ex) {
            $view = $this->view(["error" => ["code" => $ex->getCode(), "message" => $ex->getMessage()]], 400);
        }
        return $this->handleView($view);
    }

}